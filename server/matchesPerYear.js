function matchesPerYear(getData) {
  const output = getData.reduce(function (acc, cur) {
    if (acc[cur.season]) {
      acc[cur.season] += 1;
    } else {
      acc[cur.season] = 1;
    }
    return acc;
  }, {});
  return output;
}

module.exports = matchesPerYear;
