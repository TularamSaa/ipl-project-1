function matchesWonByTeamPerYear(getData) {
  const team = {};

  for (let element of getData) {
    let year = element.season;
    let winner = element.winner;
    if (team[winner]) {
      if (team[winner][year]) {
        team[winner][year] += 1;
      } else {
        team[winner][year] = 1;
      }
    } else {
      team[winner] = {};
      team[winner][year] = 1;
    }
  }

  return team;
}

module.exports = matchesWonByTeamPerYear;
