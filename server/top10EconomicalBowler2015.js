function top10EconomicalBowler2015(matchData, deliveryData, year) {
  let top10EconomyBowler = {};
  for (let eachMatch of matchData) {
    let matchYear = eachMatch["season"];
    let matchId = eachMatch["id"];
    if (matchYear == year) {
      for (let eachDelivery of deliveryData) {
        let deliveryId = eachDelivery["match_id"];
        let countBall = Number(eachDelivery["ball"]);
        let bowler = eachDelivery["bowler"];
        let runConceded = Number(eachDelivery["total_runs"]);
        // Number(eachDelivery["wide_runs"]) +
        // Number(eachDelivery["penalty_runs"]);
        if (
          matchId === deliveryId &&
          top10EconomyBowler.hasOwnProperty(bowler)
        ) {
          if (countBall > 6) {
            top10EconomyBowler[bowler]["runsConceded"] += runConceded;
          } else if (countBall < 7) {
            top10EconomyBowler[bowler]["countBall"] += 1;
            console.log(top10EconomyBowler);
            top10EconomyBowler[bowler]["runsConceded"] += runConceded;
          }
        } else if (
          matchId === deliveryId &&
          !top10EconomyBowler.hasOwnProperty(bowler)
        ) {
          top10EconomyBowler[bowler] = {};
          top10EconomyBowler[bowler]["name"] = bowler;
          if (countBall > 6) {
            top10EconomyBowler[bowler]["runsConceded"] = runConceded;
          } else if (countBall < 7) {
            top10EconomyBowler[bowler]["countBall"] = 1;
            top10EconomyBowler[bowler]["runsConceded"] = runConceded;
          }
        }
      }
    }
  }

  let arr = [];
  for (let items in top10EconomyBowler) {
    // arr.push(top10EconomyBowler[items]);
    let totalRuns = top10EconomyBowler[items]["runsConceded"];
    let overs = top10EconomyBowler[items]["countBall"] / 6;
    let Economy_Rate = totalRuns / overs;
    top10EconomyBowler[items]["Economy_Rate"] = Economy_Rate;
    arr.push(top10EconomyBowler[items]);
  }
  console.log(arr);

  arr.sort((a, b) => b.Economy_Rate - a.Economy_Rate);
  return arr.slice(0, 10);
}

module.exports = top10EconomicalBowler2015;
