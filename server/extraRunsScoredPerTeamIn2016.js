function extraRunsScoredPerTeamIn2016(matchData, deliveryData, year) {
  let extraRunsConcdededTeam = {};
  for (let each in matchData) {
    let eachData = matchData[each];
    let matchYear = eachData["season"];
    let matchId = eachData["id"];

    if (matchYear == year) {
      for (let each in deliveryData) {
        let eachDeliveryData = deliveryData[each];
        let deliveryId = eachDeliveryData["match_id"];
        let teamName = eachDeliveryData["bowling_team"];
        let extraRuns = Number(eachDeliveryData["extra_runs"]);
        // Number(eachDeliveryData["wide_runs"]) +
        // Number(eachDeliveryData["penalty_runs"]);
        if (
          deliveryId === matchId &&
          extraRunsConcdededTeam.hasOwnProperty(teamName)
        ) {
          extraRunsConcdededTeam[teamName] += extraRuns;
        } else if (
          deliveryId === matchId &&
          !extraRunsConcdededTeam.hasOwnProperty(teamName)
        ) {
          extraRunsConcdededTeam[teamName] = extraRuns;
        }
      }
    }
  }
  return extraRunsConcdededTeam;
}

module.exports = extraRunsScoredPerTeamIn2016;
