const csvFilePath = "../data/matches.csv";
const deliveryFilePath = "../data/deliveries.csv";
const csv = require("csvtojson");
let matchesPerYear = require("./matchesPerYear.js");
let matchesWonByTeamPerYear = require("./matchesWonByTeamPerYear.js");
let extraRunsScoredPerTeamIn2016 = require("./extraRunsScoredPerTeamIn2016.js");
let top10EconomicalBowler2015 = require("./top10EconomicalBowler2015.js");

csv()
  .fromFile(csvFilePath)
  .then((matchData) => {
    let matchesPerYearResult = matchesPerYear(matchData);
    matchesPerYearResult = JSON.stringify(matchesPerYearResult);

    const fs = require("fs");
    fs.writeFile(
      "../public/output/matchesPerYear.json",
      matchesPerYearResult,
      (err) => {
        if (err) console.log(err);
        else {
          console.log("File written successfully\n");
        }
      }
    );

    let matchesWonByTeamPerYearResult = matchesWonByTeamPerYear(matchData);
    matchesWonByTeamPerYearResult = JSON.stringify(
      matchesWonByTeamPerYearResult
    );

    fs.writeFile(
      "../public/output/matchesWonByTeamPerYear.json",
      matchesWonByTeamPerYearResult,
      (err) => {
        if (err) console.log(err);
        else {
          console.log("File written successfully\n");
        }
      }
    );
    csv()
      .fromFile(deliveryFilePath)
      .then((deliveryData) => {
        let extraRunsScoredPerTeamIn2016Result = extraRunsScoredPerTeamIn2016(
          matchData,
          deliveryData,
          2016
        );
        extraRunsScoredPerTeamIn2016Result = JSON.stringify(
          extraRunsScoredPerTeamIn2016Result
        );

        fs.writeFile(
          "../public/output/extraRunsScoredPerTeamIn2016.json",
          extraRunsScoredPerTeamIn2016Result,
          (err) => {
            if (err) console.log(err);
            else {
              console.log("File written successfully\n");
            }
          }
        );

        let top10EconomicalBowler2015Result = top10EconomicalBowler2015(
          matchData,
          deliveryData,
          2015
        );
        top10EconomicalBowler2015Result = JSON.stringify(
          top10EconomicalBowler2015Result
        );
        fs.writeFile(
          "../public/output/top10EconomicalBowler.json",
          top10EconomicalBowler2015Result,
          (err) => {
            if (err) console.log(err);
            else {
              console.log("File written successfully\n");
            }
          }
        );
      });
  });
